# prod/fabfile.py


import os
from fabric.contrib.files import sed
from fabric.api import env, local, run
from fabric.api import env

# initialize the base directory
abs_dir_path = os.path.dirname(
    os.path.dirname(os.path.abspath(__file__)))


# declare environment global variables

# root user
env.user = 'root'

# list of remote IP addresses
env.hosts = ['31.131.20.226']

# password for the remote server
env.password = 'fJhr234k8M'

# full name of the user
env.full_name_user = 'ElateMe Deployer'

# user group
env.user_group = 'deployers'

# user for the above group
env.user_name = 'deployer'

# ssh key path
env.ssh_keys_dir = os.path.join(abs_dir_path, 'ssh-keys')


def start_provision():
    """
    Start server provisioning
    """
    run('apt-get update -y')
    run('apt-get upgrade -y')


    # Create a new directory for a new remote server
    env.ssh_keys_name = os.path.join(
        env.ssh_keys_dir, env.host_string + '_prod_key')
    local('ssh-keygen -t rsa -b 2048 -f {0}'.format(env.ssh_keys_name))
    local('cp {0} {1}/authorized_keys'.format(
        env.ssh_keys_name + '.pub', env.ssh_keys_dir))
    # Prevent root SSHing into the remote server
    sed('/etc/ssh/sshd_config', '^UsePAM yes', 'UsePAM no')
    sed('/etc/ssh/sshd_config', '^PermitRootLogin yes',
        'PermitRootLogin no')
    sed('/etc/ssh/sshd_config', '^#PasswordAuthentication yes',
        'PasswordAuthentication no')

    install_dependencies()
    fix_supervisor_errors()
    create_deployer_group()
    create_deployer_user()
    upload_keys()
    run('service ssh reload')
    upgrade_server()


def create_deployer_group():
    """
    Create a user group for all project developers
    """
    run('groupadd {}'.format(env.user_group))
    run('mv /etc/sudoers /etc/sudoers-backup')
    run('(cat /etc/sudoers-backup; echo "%' +
        env.user_group + ' ALL=(ALL) NOPASSWD: ALL") > /etc/sudoers')
    run('chmod 440 /etc/sudoers')


def create_deployer_user():
    """
    Create a user for the user group
    """
    run('useradd -c "{}" -m -g {} {}'.format(
        env.full_name_user, env.user_group, env.user_name))
    run('passwd {}'.format(env.user_name))
    run('usermod -a -G {} {}'.format(env.user_group, env.user_name))
    run('mkdir -p /home/{}/.ssh'.format(env.user_name))
    run('chown -R {} /home/{}/.ssh'.format(env.user_name, env.user_name))
    run('chgrp -R {} /home/{}/.ssh'.format(
        env.user_group, env.user_name))


def upload_keys():
    """
    Upload the SSH public/private keys to the remote server via scp
    """
    scp_command = 'scp {} {}/authorized_keys {}@{}:~/.ssh'.format(
        env.ssh_keys_name + '.pub',
        env.ssh_keys_dir,
        env.user_name,
        env.host_string
    )
    local(scp_command)


def install_dependencies():
    """
    Install the ansible module so that Ansible
    can communicate with Ubuntu Package Manager
    """
    run('apt-get install -y software-properties-common')
    run('apt-get install -y build-essential git libjpeg-dev')
    run('add-apt-repository ppa:fkrull/deadsnakes')
    run('apt-get -y update')
    run('apt-get install -y python3.6 python3-pip')

    run('apt-add-repository -y ppa:ansible/ansible')
    run('apt-get -y update')
    run('apt-get install -y ansible')
    run('apt-get install -y python3-apt python-apt')
    run('apt-get install -y aptitude supervisor')
    run('apt-get remove -y apache2')

    # Locales

    run('locale-gen "en_US.UTF-8"')
    run('dpkg-reconfigure locales')


def fix_supervisor_errors():
    run('rm -rf /var/run/supervisor.sock')
    run('supervisord -c /etc/supervisor/supervisord.conf')
    run('supervisorctl -c /etc/supervisor/supervisord.conf')
    run('supervisorctl reread')
    run('supervisorctl update')


def upgrade_server():
    """
    Upgrade the server as a root user
    """
    run('apt-get upgrade -y')
    run('reboot')
