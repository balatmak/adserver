# Deploy guide

### Requirements
To be able to setup deploy you need

* Fabric3
* Ansible

## Provision

1. Activate virtual environment, where fabric3 is installed
2. Change variables on the top of the `bin/fabfile.py`, according to
your server installation.
3. Run:
```
    ./provision.sh
```
## Deploy

1. Activate virtual environment, where ansible is installed
2. Change variables on the top of the `bin/env_vars/base.yml`, according to
your server installation.
3. Change target hosts to deploy to in `bin/hosts`
4. Run:
```
    ./deploy.sh <path_to_private_key_to_connect_via_ssh> <server_sudo_password>
```