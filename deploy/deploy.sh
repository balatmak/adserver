#!/bin/bash

ANSIBLE_CONFIG=./ansible.cfg

ansible-playbook ./bin/deploy.yml --private-key=$1 -u deployer -v -e "ansible_sudo_pass=$2"
