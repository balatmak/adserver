from common.web import HttpRequestHandler
from tornado.web import HTTPError
from app.schemas import JSONSchema
from common.shortcuts import get_object_or_404
from pymongo import ReturnDocument

from soc_demo_groups.grouping import regroup_user

from datetime import datetime

import json
import bson.json_util


class UserListHandler(HttpRequestHandler):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.db_table = self.settings['db'].users
        self.validator = self.settings['request_validator']

    async def get(self):
        result_set = []

        async for user in self.db_table.find():
            if user.get('date_of_birth') is not None:
                user['date_of_birth'] = user['date_of_birth'].strftime('%Y-%m-%d')
            result_set.append(user)

        self.set_status(200)
        self.set_header('Content-Type', 'text/json')
        self.write(json.dumps(result_set, default=bson.json_util.default))

    async def post(self):
        user_json = json.loads(self.request.body.decode('utf-8'))

        self.validator.validate_json(user_json, JSONSchema.CREATE_USER)

        user = await self.db_table.find_one({"_id": int(user_json.get('id'))})

        if user:
            raise HTTPError(409, "User exists, use PATCH to update user profile")

        user_json["_id"] = user_json.pop("id")
        user_bson = user_json

        date = user_json.get('date_of_birth')
        if date is not None:
            user_bson['date_of_birth'] = datetime.strptime(date, '%Y-%m-%d')

        await self.db_table.insert_one(user_bson)

        self.set_status(201)
        self.finish(json.dumps(user_json, default=bson.json_util.default))
        await regroup_user(user_bson)


class UserHandler(HttpRequestHandler):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.db_table = self.settings['db'].users
        self.validator = self.settings['request_validator']

    async def get(self, user_id):
        user = await get_object_or_404(self.db_table, {"_id": int(user_id)})

        if user.get('date_of_birth') is not None:
            user['date_of_birth'] = user['date_of_birth'].strftime('%Y-%m-%d')

        self.set_header('Content-Type', 'text/json')
        self.finish(json.dumps(user, default=bson.json_util.default))

    async def patch(self, user_id):
        user = await get_object_or_404(self.db_table, query={"_id": int(user_id)})

        user_json = json.loads(self.request.body.decode('utf-8'))

        self.validator.validate_json(user_json, JSONSchema.UPDATE_USER)

        date = user_json.get('date_of_birth')
        if date is not None:
            user_json['date_of_birth'] = datetime.strptime(user_json['date_of_birth'], '%Y-%m-%d')

        updated_user = await self.db_table.find_one_and_update({'_id': int(user_id)}, {'$set': user_json},
                                                                    upsert=True, return_document=ReturnDocument.AFTER)

        self.finish(json.dumps(updated_user, default=bson.json_util.default))

        await regroup_user(updated_user)

    async def delete(self, user_id):
        user = await get_object_or_404(self.db_table, query={"_id": int(user_id)})

        await self.db_table.delete_many({"_id": int(user_id)})

        self.set_status(204)
        self.finish()


class CookieHandler(HttpRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.db_table = self.settings['db'].cookies
        self.users_table = self.settings['db'].users
        self.validator = self.settings['request_validator']

    async def post(self, user_id):
        user = await get_object_or_404(self.users_table, {"_id": int(user_id)})

        cookie_json = json.loads(self.request.body.decode('utf-8'))

        self.validator.validate_json(cookie_json, JSONSchema.COOKIE)

        self.db_table.save({"user_id": int(user_id), "cookie": cookie_json["cookie"]})

        self.set_status(204)
        self.finish()
