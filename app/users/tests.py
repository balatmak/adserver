from common.tests import BaseHTTPTestCase

import json


class UsersManagementTests(BaseHTTPTestCase):

    def test_user_create(self):
        user_id = 4
        self.fetch(f'/user/{user_id}/', method='DELETE')

        user_json = {
            "id": user_id,
            "gender": "M",
            "email": "mbalatsko@gmail.com",
            "date_of_birth": "1999-09-09"
        }

        response = self.fetch('/users/', method='POST',
                              body=json.dumps(user_json))

        self.assertEqual(response.code, 201)
        result = json.loads(response.body.decode('utf-8'))

        self.assertTrue("_id" in result)
        self.assertEqual(user_json["gender"], result["gender"])

    def test_user_update(self):
        user_id = 4
        self.fetch(f'/user/{user_id}/', method='DELETE')

        user_json = {
            "id": user_id,
            "gender": "M",
            "email": "mbalatsko@gmail.com",
            "date_of_birth": "1999-09-09"
        }

        response = self.fetch('/users/', method='POST',
                              body=json.dumps(user_json))

        result = json.loads(response.body.decode('utf-8'))
        self.assertEqual(user_json["gender"], result["gender"])

        user_json["gender"] = "F"

        user_json.pop("id")

        response = self.fetch(f'/user/{user_id}/', method='PATCH',
                              body=json.dumps(user_json))

        result = json.loads(response.body.decode('utf-8'))
        self.assertEqual(user_json["gender"], result["gender"])

    def test_user_delete(self):
        user_id = 4
        self.fetch(f'/user/{user_id}/', method='DELETE')

        group_json = {
            "id": user_id,
            "gender": "M",
            "email": "mbalatsko@gmail.com",
            "date_of_birth": "1999-09-09"
        }

        response = self.fetch('/users/', method='POST',
                              body=json.dumps(group_json))

        self.assertEqual(response.code, 201)
        result = json.loads(response.body.decode('utf-8'))

        self.assertTrue("_id" in result)
        self.assertEqual(group_json["gender"], result["gender"])

        response = self.fetch(f'/user/{user_id}/', method='DELETE')

        self.assertEqual(response.code, 204)
