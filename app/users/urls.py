from .handlers import UserHandler, UserListHandler, CookieHandler

urlpatterns = [

    (r'^/users/$', UserListHandler),
    (r'^/user/(\d+)/$', UserHandler),

    (r'^/user/(\d+)/add-cookie$', CookieHandler),
]