from app.settings import db
from common.util import find_first_in_list

import math
from typing import List, Dict

from .exceptions import NoRatingsError


class Advisor:

    def __init__(self, user):
        self._db = db

        self._user = user

        self._user_ratings = {}
        self._user_rated_products = []

        self._user_avg_rating = user.get('avg_rating')
        if self._user_avg_rating is None:
            raise NoRatingsError("User hasn't rated any product yet")

        for rating in user.get('ratings', []):
            self._user_ratings[rating['id']] = rating['value']
            self._user_rated_products.append(rating['id'])

        self._similarity_dict = {}

    async def recommend(self, limit: int) -> List[Dict]:
        resultset = []
        async for product in self._db.products.find({"_id": {'$nin': self._user_rated_products}}):
            prediction = await self._predict(product['_id'])
            if prediction > 0.5:
                product['prediction'] = prediction
                resultset.append(product)

        resultset = sorted(resultset, key=lambda k: k['prediction'], reverse=True)
        return resultset[:limit]

    def _similarity(self, user_to_compare) -> float:

        if self._similarity_dict.get(user_to_compare.get('_id')) is not None:
            return self._similarity_dict[user_to_compare.get('_id')]

        result = 0
        max_distance = 0

        for rating in user_to_compare.get('ratings', []):

            if self._user_ratings.get(rating['id']) is None:
                continue

            result += (self._user_ratings[rating['id']] - rating['value'])**2
            max_distance += 1

        max_distance = math.sqrt(max_distance)
        result = max_distance - math.sqrt(result)

        self._similarity_dict[user_to_compare.get('_id')] = result
        return result

    async def _predict(self, product_id) -> float:

        weighted_arithmetic_mean_numerator = 0
        weighted_arithmetic_mean_denominator = 0

        async for user in self._db.users.find({'ratings': {'$elemMatch': {'id': product_id}}}):
            user_avg_rating = user.get('avg_rating')
            if user_avg_rating is None:
                continue

            similarity = self._similarity(user)

            user_product_rating = find_first_in_list(user['ratings'], key='id', value=product_id)['value']

            weighted_arithmetic_mean_numerator += similarity*(user_product_rating-user_avg_rating)
            weighted_arithmetic_mean_denominator += similarity

        if weighted_arithmetic_mean_denominator == 0:
            return self._user_avg_rating

        return self._user_avg_rating + (weighted_arithmetic_mean_numerator/weighted_arithmetic_mean_denominator)
