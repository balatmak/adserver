from .handlers import RecommendationHandler, ActionHandler

urlpatterns = [
    (r'^/recommend/(\d+)/$', RecommendationHandler),

    (r'^/user/(\d+)/product/(\d+)/action/(\w+)/$', ActionHandler)
]