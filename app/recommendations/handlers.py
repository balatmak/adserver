from common.web import HttpRequestHandler
from common.shortcuts import get_object_or_404
from common.util import find_first_in_list

from tornado.web import HTTPError
from tornado.web import MissingArgumentError

from .recomendation import Advisor

import json
import bson.json_util


class RecommendationHandler(HttpRequestHandler):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.users_table = self.settings['db'].users
        self.products_table = self.settings['db'].products
        self.validator = self.settings['request_validator']

    async def get(self, user_id):

        user = await get_object_or_404(self.users_table, {"_id": int(user_id)})

        try:
            search_query = self.get_argument('q')
        except MissingArgumentError:
            search_query = None

        limit = self.get_argument('limit', 10)

        resultset = []
        if search_query:
            products_list = self.products_table.find(
                {
                    "$text":
                        {
                            "$search": f'{search_query}'
                        }
                },
                {
                    "score":
                        {
                            "$meta": "textScore"
                        }
                }
            ).sort([("textScore", 1)]).limit(limit)

            async for product in products_list:
                resultset.append(product)

        else:
            advisor = Advisor(user)
            recommendations = await advisor.recommend(limit)

            if len(recommendations) < limit and user.get('soc_demo_groups'):
                async for product in self.products_table.find({ 'soc_demo_groups' : { '$in' : user['soc_demo_groups'] }}):
                    product.pop('soc_demo_groups')
                    recommendations.append(product)
                    if len(recommendations) < limit:
                        break

            resultset = recommendations

        self.set_status(200)
        self.set_header('Content-Type', 'text/json')
        self.write(json.dumps(resultset[:limit], default=bson.json_util.default))


class ActionHandler(HttpRequestHandler):

    ACTION_LIST = {
        "hide_wish": 0.1,
        "collect_1_4": 0.25,
        "close_wish": 0.5,
        "collect_1_2": 0.5,
        "collect_3_4": 0.75,
        "donate": 0.8,
        "collect_full": 1
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.users_table = self.settings['db'].users
        self.products_table = self.settings['db'].products

    async def post(self, user_id, product_id, action):
        new_rating = self.ACTION_LIST.get(action)
        if new_rating is None:
            raise HTTPError(404, "Action does not exist")

        user = await get_object_or_404(self.users_table, {'_id': int(user_id)})
        product = await get_object_or_404(self.products_table, {'_id': int(product_id)})

        ratings = user.get('ratings', [])
        rating_sum = 0
        rating_cnt = len(ratings)
        rating_changed = None

        for rating in ratings:
            if rating['id'] == int(product_id):
                if rating['value'] > new_rating:
                    rating_changed = True
                    rating['value'] = new_rating
                else:
                    rating_changed = False
            rating_sum += rating['value']

        if rating_changed is None:
            rating_cnt += 1
            rating_sum += new_rating
            user['avg_rating'] = (rating_sum/rating_cnt)
            if len(ratings) > 0:
                user['ratings'].append({'id': int(product_id), 'value': new_rating})
            else:
                user['ratings'] = [{'id': int(product_id), 'value': new_rating}, ]
        elif rating_changed:
            user['avg_rating'] = (rating_sum / rating_cnt)

        self.users_table.update({'_id': int(user_id)},
                                {'$set': user})

        self.set_status(204)
        self.finish()
