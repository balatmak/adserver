from .handlers import SocDemoGroupHandler, SocDemoGroupListHandler

urlpatterns = [
    (r'^/soc-demo-groups/$', SocDemoGroupListHandler),
    (r'^/soc-demo-group/(\d+)/$', SocDemoGroupHandler)
]
