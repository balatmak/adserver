from app.settings import db

from common.util import calculate_age


def check_user_group_rules(user, group) -> bool:
    age_rules = group['rules'].get('age')
    gender_rules = group['rules'].get('gender')
    place_of_birth_rules = group['rules'].get('place_of_birth')

    score = 0

    if user.get('soc_demo_groups') is None:
        user['soc_demo_groups'] = []

    if age_rules is None:
        score += 1
    else:
        if user.get('date_of_birth') is not None:
            user_age = calculate_age(user['date_of_birth'])
            for rule in age_rules:
                if rule['from'] <= user_age <= rule['to']:
                    score += 1
                    break
        if score < 1:
            return False

    if gender_rules is None:
        score += 1
    else:
        if user.get('gender') is not None:
            user_gender = user['gender']
            for rule in gender_rules:
                if rule == user_gender:
                    score += 1
                    break
        if score < 2:
            return False

    if place_of_birth_rules is None:
        score += 1
    else:
        if user.get('place_of_birth') is not None:
            user_place_of_birth = user['place_of_birth']
            for rule in place_of_birth_rules:
                if rule == user_place_of_birth:
                    score += 1
                    break
        if score < 3:
            return False

    return True


async def regroup_user(user) -> None:
    soc_demo_table = db.soc_demo_groups
    users_table = db.users

    user['soc_demo_groups'] = []

    async for group in soc_demo_table.find():
        if check_user_group_rules(user, group):
            user['soc_demo_groups'].append(group['_id'])

    await users_table.find_one_and_update({'_id': int(user['_id'])}, {'$set': user})


async def recalculate_group_users(group):
    users_table = db.users

    async for user in users_table.find():

        if check_user_group_rules(user, group):
            if user.get('soc_demo_groups') is not None:
                new_soc_demo_set = set(user['soc_demo_groups'])
                new_soc_demo_set.add(group['_id'])
                user['soc_demo_groups'] = list(new_soc_demo_set)
            else:
                user['soc_demo_groups'] = [group['_id'], ]
        else:
            if user.get('soc_demo_groups') is not None:
                try:
                    user['soc_demo_groups'].remove(group['_id'])
                except ValueError:
                    pass
        await users_table.find_one_and_update({'_id': int(user['_id'])}, {'$set': user})
