from common.web import HttpRequestHandler
from common.shortcuts import get_object_or_404
from common.db import generate_id
from pymongo import ReturnDocument

from app.schemas import JSONSchema
from .grouping import recalculate_group_users

import json
import bson.json_util


class SocDemoGroupHandler(HttpRequestHandler):

    regroup = None
    cleanup = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.db_table = self.settings['db'].soc_demo_groups
        self.validator = self.settings['request_validator']

    async def get(self, group_id):
        group = await get_object_or_404(self.db_table, {'_id': int(group_id)})

        self.set_header('Content-Type', 'text/json')
        self.finish(json.dumps(group, default=bson.json_util.default))

    async def patch(self, group_id):
        group = await get_object_or_404(self.db_table, {'_id': int(group_id)})

        group_json = json.loads(self.request.body.decode('utf-8'))

        self.validator.validate_json(group_json, JSONSchema.UPDATE_GROUP)

        group = await self.db_table.find_one_and_update({'_id': int(group_id)}, {'$set': group_json}, upsert=True,
                                                        return_document=ReturnDocument.AFTER)

        if group_json.get('rules') is not None:
            self.regroup = group

        self.set_header('Content-Type', 'text/json')
        self.finish(json.dumps(group, default=bson.json_util.default))

        if group_json.get('rules') is not None:
            await recalculate_group_users(group)

    async def delete(self, group_id):
        group = await get_object_or_404(self.db_table, {'_id': int(group_id)})

        await self.db_table.delete_many({"_id": int(group_id)})

        self.cleanup = int(group_id)

        self.set_status(204)
        self.finish()
        await self.settings['db'].users.update({},
                                               {'$pull': {'soc_demo_groups': self.cleanup}},
                                               multi=True, upsert=False)


class SocDemoGroupListHandler(HttpRequestHandler):

    regroup = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.db_table = self.settings['db'].soc_demo_groups
        self.validator = self.settings['request_validator']

    async def get(self):
        result_set = []

        async for group in self.db_table.find():
            result_set.append(group)

        self.set_status(200)
        self.set_header('Content-Type', 'text/json')
        self.finish(json.dumps(result_set, default=bson.json_util.default))

    async def post(self):
        group_json = json.loads(self.request.body.decode('utf-8'))

        self.validator.validate_json(group_json, JSONSchema.CREATE_GROUP)

        group_json['_id'] = await generate_id('soc_demo_groups')

        await self.db_table.insert_one(group_json)

        self.regroup = group_json

        self.set_status(201)
        self.set_header('Content-Type', 'text/json')
        self.finish(group_json)
        await recalculate_group_users(self.regroup)
