from common.tests import BaseHTTPTestCase
import json


class SocDemoGroupsManagementTests(BaseHTTPTestCase):

    def test_group_create(self):
        group_json = {
            "name": "Students",
            "rules": {
                "age": [
                    {
                        "from": 17,
                        "to": 26
                    }
                ]
            }
        }

        response = self.fetch('/soc-demo-groups/', method='POST',
                              body=json.dumps(group_json))

        self.assertEqual(response.code, 201)
        result = json.loads(response.body.decode('utf-8'))

        self.assertTrue("_id" in result)
        self.assertEqual(group_json["name"], result["name"])

    def test_group_delete(self):
        group_json = {
            "name": "Students",
            "rules": {
                "age": [
                    {
                        "from": 17,
                        "to": 26
                    }
                ]
            }
        }

        response = self.fetch('/soc-demo-groups/', method='POST',
                              body=json.dumps(group_json))
        result = json.loads(response.body.decode('utf-8'))

        result_id = int(result.get("_id"))
        response = self.fetch(f'/soc-demo-group/{result_id}/', method='DELETE')
        self.assertEqual(response.code, 204)

    def test_group_update(self):
        group_json = {
            "name": "Students",
            "rules": {
                "age": [
                    {
                        "from": 17,
                        "to": 26
                    }
                ]
            }
        }

        response = self.fetch('/soc-demo-groups/', method='POST',
                              body=json.dumps(group_json))
        result_id = int(json.loads(response.body.decode('utf-8')).get("_id"))

        group_json["name"] = "AAA"
        response = self.fetch(f'/soc-demo-group/{result_id}/', method='PATCH',
                              body=json.dumps(group_json))

        result = json.loads(response.body.decode('utf-8'))

        self.assertEqual(group_json["name"], result["name"])