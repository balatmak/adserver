from products import products_urls
from recommendations import reccomendations_urls
from users import user_urls
from soc_demo_groups import soc_demo_groups_urls

urlpatterns = [

] + products_urls + reccomendations_urls + user_urls + soc_demo_groups_urls

