from enum import Enum


class JSONSchema(Enum):
    CREATE_USER = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "additionalProperties": False,
        "properties": {
            "id": {
                "type": "integer"
            },
            "email": {
                "type": "string",
                "format": "email"
            },
            "date_of_birth": {
                "type": "string",
                "format": "date"
            },
            "place_of_birth": {
                "type": "string"
            },
            "gender": {
                "type": "string",
                "enum": [
                    "M",
                    "F"
                ]
            }
        },
        "required": [
            "id"
        ]
    }

    UPDATE_USER = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "additionalProperties": False,
        "properties": {
            "email": {
                "type": "string",
                "format": "email"
            },
            "date_of_birth": {
                "type": "string",
                "format": "date"
            },
            "place_of_birth": {
                "type": "string"
            },
            "gender": {
                "type": "string",
                "enum": [
                    "M",
                    "F"
                ]
            }
        }
    }

    COOKIE = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "additionalProperties": False,
        "properties": {
            "cookie": {
                "type": "string"
            },
        },
        "required": ["cookie", ]
    }

    PRODUCTS_LIST = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "array",
        "additionalProperties": False,
        "properties": {
            "image": {
                "type": "string"
            },
            "name": {
                "type": "string"
            },
            "description": {
                "type": "string"
            },
            "price": {
                "type": "number",
                "minimum": 0,
                "exclusiveMinimum": True
            }
        },
        "required": [
            "image",
            "name",
            "description",
            "price"
        ]
    }

    UPDATE_PRODUCT = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "additionalProperties": False,
        "properties": {
            "image": {
                "type": "string"
            },
            "name": {
                "type": "string"
            },
            "description": {
                "type": "string"
            },
            "price": {
                "type": "number",
                "minimum": 0,
                "exclusiveMinimum": True
            },
            "soc_demo_groups": {
                "type": "array",
                "items": {
                    "type": "number"
                }
            }
        }
    }

    CREATE_GROUP = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "additionalProperties": False,
        "properties": {
            "name": {
                "type": "string"
            },
            "rules": {
                "type": "object",
                "properties": {
                    "age": {
                        "type": "array",
                        "uniqueItems": True,
                        "items": {
                            "type": "object",
                            "additionalProperties": False,
                            "properties": {
                                "from": {
                                    "type": "integer"
                                },
                                "to": {
                                    "type": "integer"
                                }
                            },
                            "arrayItem": True
                        }
                    },
                    "gender": {
                        "type": "array",
                        "items": {
                            "type": "string",
                            "arrayItem": True,
                            "enum": [
                                "M",
                                "F"
                            ]
                        }
                    },
                    "place_of_birth": {
                        "type": "array",
                        "uniqueItems": True,
                        "items": {
                            "type": "string",
                            "arrayItem": True
                        }
                    }
                }
            }
        },
        "required": [
            "name",
            "rules"
        ]
    }

    UPDATE_GROUP = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "additionalProperties": False,
        "properties": {
            "name": {
                "type": "string"
            },
            "rules": {
                "type": "object",
                "properties": {
                    "age": {
                        "type": "array",
                        "uniqueItems": True,
                        "items": {
                            "type": "object",
                            "additionalProperties": False,
                            "properties": {
                                "from": {
                                    "type": "integer"
                                },
                                "to": {
                                    "type": "integer"
                                }
                            },
                            "arrayItem": True
                        }
                    },
                    "gender": {
                        "type": "array",
                        "items": {
                            "type": "string",
                            "arrayItem": True,
                            "enum": [
                                "M",
                                "F"
                            ]
                        }
                    },
                    "place_of_birth": {
                        "type": "array",
                        "uniqueItems": True,
                        "items": {
                            "type": "string",
                            "arrayItem": True
                        }
                    }
                }
            }
        }
    }


class XMLSchema(Enum):
    PRODUCTS_LIST = """
        <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
            <xs:element name="products">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name="product" maxOccurs="unbounded">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name="image" type="xs:anyURI"/>  
                          <xs:element name="name" type="xs:string"/>
                          <xs:element name="description" type="xs:string"/>
                          <xs:element name="price" type="xs:decimal"/>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
            </xs:element>  
        </xs:schema>
        """
