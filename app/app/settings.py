import motor.motor_tornado
client = motor.motor_tornado.MotorClient('localhost', 27017)
db = client.elatemedb


from common.validators import RequestValidator
from .schemas import XMLSchema, JSONSchema
request_validator = RequestValidator(xml_schema_enum=XMLSchema, json_schema_enum=JSONSchema)

from .urls import urlpatterns as urls

settings = {
    "db": db,
    "request_validator": request_validator,

    "handlers": urls
}
