import xml.etree.ElementTree as ET
from typing import Dict, List


class ProductParser:

    @staticmethod
    def products_list_to_dict(xml_list_string: str) -> List[Dict]:
        """
         Converts string of xml products list to json products list
        :param xml_list_string: string xml products list
        :return: list of converted products xml list to json format
        """
        result_json = []

        list_root = ET.fromstring(xml_list_string)
        for product in list_root:
            product_dict = {}

            product_dict["image"] = product.find("image").text
            product_dict["name"] = product.find("name").text
            product_dict["description"] = product.find("description").text
            product_dict["price"] = float(product.find("price").text)

            result_json.append(product_dict)

        return result_json
