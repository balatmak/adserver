from tornado.testing import gen_test

from common.tests import BaseHTTPTestCase
import json


class ProductsManagementTests(BaseHTTPTestCase):

    def test_products_json_import(self):
        products_json_list = [
            {
                "image": "some.url/imgae.jpg",
                "name": "Iphone",
                "description": "Lorem",
                "price": 10000.0
            },
            {
                "image": "some.url/imgae.jpg",
                "name": "Mi FIT",
                "description": "Lorem",
                "price": 999.9
            }]

        response = self.fetch('/products/', method='POST',
                              body=json.dumps(products_json_list))
        resultlist = json.loads(response.body.decode('utf-8'))

        i = 0
        for item in resultlist:
            self.assertTrue('_id' in item)
            self.assertEquals(item['name'], products_json_list[i]['name'])
            i += 1

        self.assertEqual(response.code, 201)

    def test_products_xml_import(self):
        products_xml_list = """<products>
    <product>
        <image>ww.dd</image>
        <name>Product 3</name>
        <description>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aliquid amet consequatur
            cupiditate dolores magni molestiae, nisi optio placeat possimus quo vitae. Culpa ducimus excepturi possimus
            quisquam sed voluptas?
        </description>
        <price>1000.0</price>
    </product>
    <product>
        <image>ww.aa</image>
        <name>Product 1</name>
        <description>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aliquid amet consequatur
            cupiditate dolores magni molestiae, nisi optio placeat possimus quo vitae. Culpa ducimus excepturi possimus
            quisquam sed voluptas?
        </description>
        <price>2000.0</price>
    </product>
    <product>
        <image>ww.dd</image>
        <name>Product 2</name>
        <description>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aliquid amet consequatur
            cupiditate dolores magni molestiae, nisi optio placeat possimus quo vitae. Culpa ducimus excepturi possimus
            quisquam sed voluptas?
        </description>
        <price>10078.0</price>
    </product>
</products>
        """
        response = self.fetch('/products/?format=xml', method='POST',
                              body=products_xml_list)
        resultlist = json.loads(response.body)

        i = 0
        for item in resultlist:
            self.assertTrue('_id' in item)

        self.assertEqual(response.code, 201)

    def test_products_details(self):
        products_json_list = [
            {
                "image": "some.url/imgae.jpg",
                "name": "Iphone",
                "description": "Lorem",
                "price": 10000.0
            }
        ]

        response = self.fetch('/products/', method='POST',
                              body=json.dumps(products_json_list))
        resultlist = json.loads(response.body)

        for item in resultlist:
            product_id = int(item['_id'])
            response = self.fetch(f'/products/{product_id}/')
            product = json.loads(response.body)
            self.assertEquals(item['name'], product['name'])
            self.assertEqual(response.code, 200)

    def test_product_does_not_exist(self):
        response = self.fetch('/products/0/')
        self.assertEquals(response.code, 404)

    def test_product_update(self):
        products_json_list = [
            {
                "image": "some.url/imgae.jpg",
                "name": "Iphone",
                "description": "Lorem",
                "price": 10000.0
            }
        ]

        response = self.fetch('/products/', method='POST',
                              body=json.dumps(products_json_list))
        resultlist = json.loads(response.body)

        for item in resultlist:
            product_id = int(item['_id'])
            update = {
                "name": "Update name"
            }
            response = self.fetch(f'/products/{product_id}/', method='PATCH',
                                  body=json.dumps(update))
            product = json.loads(response.body)
            self.assertEquals(product['name'], update["name"])
            self.assertEqual(response.code, 200)

    def test_product_delete(self):

        products_json_list = [
            {
                "image": "some.url/imgae.jpg",
                "name": "Iphone",
                "description": "Lorem",
                "price": 10000.0
            }
        ]

        response = self.fetch('/products/', method='POST',
                              body=json.dumps(products_json_list))
        resultlist = json.loads(response.body)

        for item in resultlist:
            product_id = int(item['_id'])
            response = self.fetch(f'/products/{product_id}/', method='DELETE')
            self.assertEqual(response.code, 204)
