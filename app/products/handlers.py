from common.web import HttpRequestHandler
from tornado.web import HTTPError
from common.db import safe_insert_many
from app.schemas import XMLSchema, JSONSchema
from .parsers import ProductParser
from common.shortcuts import get_object_or_404

import json
import bson.json_util

from pymongo import ReturnDocument


class ProductsListHandler(HttpRequestHandler):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.db_table = self.settings['db'].products
        self.validator = self.settings['request_validator']

    async def get(self):
        """
         Responds with a list of all products stored in database 
        """
        resultset = []

        async for product in self.db_table.find():
            resultset.append(product)

        self.set_header('Content-Type', 'text/json')
        self.write(json.dumps(resultset, default=bson.json_util.default))

    async def post(self):
        """
         Takes products list in json or xml format.
         Responds with 201 status and the inserted set of products in json format.
        """
        request_format = self.get_argument('format', default='json', strip=False)

        if request_format == 'xml':

            xml_products_list = self.request.body.decode('utf-8')

            self.validator.validate_xml(xml_products_list, XMLSchema.PRODUCTS_LIST)

            products_json = ProductParser.products_list_to_dict(xml_products_list)

        elif request_format == 'json':

            products_json = json.loads(self.request.body.decode('utf-8'))

            self.validator.validate_json(products_json, JSONSchema.PRODUCTS_LIST)

        else:
            raise HTTPError(400, f'{request_format} is not supported')

        inserted_set = await safe_insert_many('products', products_json, autoincrement_id=True)

        self.set_status(201)
        self.finish(json.dumps(inserted_set))


class ProductHandler(HttpRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.db_table = self.settings['db'].products
        self.validator = self.settings['request_validator']

    async def get(self, product_id):
        """
         Responds with the found product by id in the database or responds with 404,
         if product does not exist.
        :param product_id: id of the requested product 
        """

        product = await get_object_or_404(self.db_table, query={"_id": int(product_id)})

        self.set_header('Content-Type', 'text/json')
        self.finish(json.dumps(product, default=bson.json_util.default))

    async def patch(self, product_id):
        """
         Responds with the updated product found by id in the database and updated with the received json,
         or responds with 404, if product does not exist.
         
        :param product_id: id of the requested product 
        """
        product = await get_object_or_404(self.db_table, query={"_id": int(product_id)})

        product_json = json.loads(self.request.body.decode('utf-8'))

        self.validator.validate_json(product_json, JSONSchema.UPDATE_PRODUCT)

        updated_product = await self.db_table.find_one_and_update({'_id': int(product_id)}, {'$set': product_json},
                                                                  upsert=True, return_document=ReturnDocument.AFTER)

        self.set_header('Content-Type', 'text/json')
        self.finish(json.dumps(updated_product, default=bson.json_util.default))

    async def delete(self, product_id):
        """
         Deletes the product by the id and responds with 204 status,
         or responds with 404, if product does not exist.
        """
        product = await get_object_or_404(self.db_table, query={"_id": int(product_id)})

        await self.db_table.delete_many({"_id": int(product_id)})

        self.set_status(204)
        self.finish()

