from .handlers import ProductsListHandler, ProductHandler

urlpatterns = [
    (r'^/products/$', ProductsListHandler),
    (r'^/products/(\d+)/$', ProductHandler)
]
