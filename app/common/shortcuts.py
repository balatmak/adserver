from tornado.web import HTTPError

async def get_object_or_404(collection, query):
    obj = await collection.find_one(query)

    if obj is None:
        raise HTTPError(404, 'Object does not exist')

    return obj
