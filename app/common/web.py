import json
from tornado.web import RequestHandler, HTTPError


class HttpRequestHandler(RequestHandler):

    def write_error(self, status_code=400, **kwargs):
        self.set_header('Content-Type', 'text/json')
        type, exc, traceback = kwargs.get("exc_info")
        self.finish(json.dumps({
            'error': {
                'code': status_code,
                'message': exc.log_message
            }
        }))