from enum import Enum
from jsonschema import validate, ValidationError
from tornado.web import HTTPError
from lxml import etree


class RequestValidator:

    def __init__(self, xml_schema_enum=None, json_schema_enum=None):
        if not issubclass(xml_schema_enum, Enum):
            raise ValueError('xml_schema_enum is not a subclass of Enum')
        if not issubclass(json_schema_enum, Enum):
            raise ValueError('json_schema_enum is not a subclass of Enum')

        self._xml_schema = xml_schema_enum
        self._json_schema = json_schema_enum

    def validate_json(self, validation_data, schema):
        if schema not in self._json_schema:
            raise ValueError('schema object is not in available json schema list')

        schema_value = schema.value

        try:
            validate(validation_data, schema_value)
        except ValidationError as e:
            raise HTTPError(400, str(e))

    def validate_xml(self, validation_data, schema):
        if schema not in self._xml_schema:
            raise ValueError('schema object is not in available xml schema list')

        input_schema = schema.value

        schema_doc = etree.XML(input_schema)
        schema = etree.XMLSchema(schema_doc)
        parser = etree.XMLParser(schema=schema)

        try:
            doc = etree.fromstring(validation_data, parser)
        except etree.XMLSyntaxError as e:
            raise HTTPError(400, str(e))
