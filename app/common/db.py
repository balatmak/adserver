from tornado.web import HTTPError

from app.settings import db

from typing import List, Dict

async def generate_id(collection: str) -> int:
    """
     Fakes autoincrement id generation in MongoDB collections 
    :param collection: mongo db collection to generate id for
    :return: generated id for collection from the sequence table 
    """

    collection_sequence = await db.seqs.find_and_modify(
        query={'_id': collection + "id"},
        update={
            '$inc': {'seq': 1},
        },
        new=True
    )

    return collection_sequence.get('seq')


async def safe_insert_many(collection: str, lst: list, autoincrement_id=False):
    """

    :param collection: 
    :param lst: 
    :param autoincrement_id: flag 
    :return: 
    """

    db_collection = db[collection]
    rollback_items = []
    inserted_items = []

    for item in lst:
        if autoincrement_id:
            item["_id"] = await generate_id(collection)
        result = await db_collection.insert_one(item)

        if result is None:
            await db_collection.delete_many({'$or': rollback_items})
            raise HTTPError(500, "Error occurred while many insert into db, ROLLBACK")
        else:
            rollback_items.append({"_id": result.inserted_id})
        inserted_items.append(item)

    return inserted_items
