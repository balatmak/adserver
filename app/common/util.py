from datetime import date


def calculate_age(date_of_birth: date) -> int:
    today = date.today()
    return today.year - date_of_birth.year - ((today.month, today.day) < (date_of_birth.month, date_of_birth.day))


def find_first_in_list(lst: list, key, value):
    try:
        result = (item for item in lst if item[key] == value).__next__()
    except StopIteration:
        return None
    return result

