from tornado.testing import AsyncHTTPTestCase
from tornado.ioloop import IOLoop

from tornado import web

from app.settings import settings


class BaseHTTPTestCase(AsyncHTTPTestCase):
    def get_app(self):
        import motor.motor_tornado
        client = motor.motor_tornado.MotorClient('localhost', 27017)
        settings['db'] = client.elatemedb
        return web.Application(**settings)

    def get_new_ioloop(self):
        return IOLoop.instance()
