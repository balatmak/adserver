#!/usr/bin/env python
from tornado import web, ioloop
from tornado.options import define, options, parse_command_line

from app.settings import settings

define(name='port', default="5555", help="Port to launch application on")


def make_app():
    return web.Application(**settings)

if __name__ == "__main__":
    parse_command_line()
    app = make_app()
    app.listen(options.port)
    ioloop.IOLoop.current().start()
