/**
 * Created by max on 25.4.17.
 */

db = db.getSiblingDB('elatemedb');

db.seqs.findAndModify({
    query: { _id: "productsid" },
    update: {
        $setOnInsert: { seq: 0 }
    },
    new: true,   // return new doc if one is upserted
    upsert: true // insert the document if it does not exist
});