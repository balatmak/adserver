# Installation guide

###### Advert server installation guide on Ubuntu 14.04 OS.

### Requirements
To be able to setup and run project you need

* MongoDB 3.4
* Python 3.6
* pip
* virtualenv

### Install dependencies
MongoDB 3.4
```
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
    echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
    sudo apt-get update
    sudo apt-get install mongodb-org
```
Python 3.6
```
    sudo add-apt-repository ppa:fkrull/deadsnakes
    sudo apt-get update
    sudo apt-get install python3.6
```
pip
```
    sudo apt-get install python3-pip
```
virtualenv
```
    sudo pip install virtualenv
```
### Setup

Syncronize database indexes and sequences:
```
    cd mongo_scripts/
    ./syncdb.sh
```
Go to `app` folder and create virtual environment:
```
    cd ../app
    virtualenv -p /usr/bin/python3.6 venv
```
To begin using the virtual environment, it needs to be activated:
```
    source venv/bin/activate
```
Install requirements inside virtual environment:
```
    pip install -r requirements.txt
```
Now you should be able to run project locally:
```
    ./manage.py
```
or
```
    ./manage.py --port=<port_to_run_app_on>
```
Default port is 5555.
Server should be running on [localhost:5555](http://localhost:5555)